const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const session = require('express-session')
const bodyParser = require('body-parser')
const passport = require('passport')
const UserModel = require('./models/User') // Maybe it would be better to require it after connection?
const app = express()
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/autodialer', console.error.bind(console,'DB CONNECTION FAILURE: '));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: 't47yhw3as34a',
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'web-phone/build')));
passport.use(UserModel.createStrategy());
passport.serializeUser(UserModel.serializeUser());
passport.deserializeUser(UserModel.deserializeUser());
app.use(passport.initialize());
app.use(passport.session());


app.use('/user', require('./routes/user')); // Handles login
app.use('/api', require('./routes/api'));

app.all('*',function(req,res,next){
	console.log('Is authenticated: '+req.isAuthenticated());
    if(req.isAuthenticated()){
        next();
    }else{
        res.redirect('/user/login')
        // next(new Error(401)); // 401 Not Authorized
    }
});

app.use('/', require('./routes/index'));

app.use('/scenario', require('./routes/scenario'));




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
