# Api snippets

## Create user:
```
curl -X POST -H 'Content-Type: application/json' --data '{ "username":"user", "password":"2244", "sip_uri": "1000@192.168.199.128", "sip_username": "1000", "sip_password": "1234", "sip_server": "ws://192.168.199.128:5066" }' http://localhost:3000/api/users
```

## Create campaign with clients:
```
curl -X POST -H 'Content-Type: application/json' --data '{ "name":"Test campaign", "template":"<h1>{{ displayname }}</h1> <h2> {{ caption }} </h2> <h3> iframe: {{ iframesrc }} </h3> <h3> img: {{ imguri }} </h3> <iframe src=\"{{ iframesrc }}\" ></iframe> <img src=\"{{ imguri }}\" />", "isEnabled": true, "clients": [{ "displayname": "Demo client", "contact": "113", "iframesrc": "http://miem.hse.ru/", "imguri":"https://imgs.xkcd.com/comics/travelling_salesman_problem.png", "caption": "Это тестовый клиент"}]}' http://localhost:3000/api/campaigns
```