const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');

const User = new Schema({
    username: { type: String, required: true },
    password: String,
    sip_uri: String,
    sip_username: String,
    sip_password: String,
    sip_server: String,
});

User.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', User);
