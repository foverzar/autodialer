const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      ClientSchema = require('./ClientSchema');

const Campaign = new Schema({
    name: { type: String, required: true },
    template: { type: String, required: true },
    isEnabled: { type: Boolean, required: true },
    clients: {type: [ClientSchema], default: []}
});

module.exports = mongoose.model('Campaign', Campaign);
