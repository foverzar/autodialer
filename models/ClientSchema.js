const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

const Client = new Schema({
    displayname: { type: String, required: true },
    contact: {type: String, required: true},
    contact_status_code: { type: Number, default: 0 },
    last_contact: Date,
    next_contact: Date,
},{
    strict: false
});


module.exports = Client;
