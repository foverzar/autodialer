const express = require('express'),
      router = express.Router(),
      User = require('../../models/User');


router.route('/current').get((req, res) => {
    res.json(req.user);
})

router.route('/').get((req, res) => {
    User.find((err, users) => {
        if (!err) {
            return res.json(users);
        } else {
            return res.send(err);
        }
    }); 
})
.post((req, res) => {
    User.register(new User({
        username: req.body.username,
        sip_uri: req.body.sip_uri,
        sip_username: req.body.sip_username,
        sip_password: req.body.sip_password,
        sip_server: req.body.sip_server,
    }), req.body.password, function(err) {
        if (err) {
            return res.status(400).send(err);
        }
        res.sendStatus(200);
    })
})

router.route('/:id').get((req, res) => {
    User.findOne({ _id: req.params.id }, (err, user) => {
        if (err) {
            return res.send(err);
        }
    
        res.json(user);

    });
})
.patch((req,res) => {
    User.findOne({ _id: req.params.id }, (err, user) => {
        if (err) {
            return res.send(err)
        }

        for (prop in req.body) {
            user[prop] = req.body[prop];
        }

        user.save((err) => {
            if (err) {
                return res.send(err);
            }
        });

        res.send(200);

    });
})
.delete((req,res) => {
    User.remove({ _id: req.params.id }, (err) => {
        if (err) {
            return res.send(err);
        }
        res.send(200);
    });
});

module.exports = router;