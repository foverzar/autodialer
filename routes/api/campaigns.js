const express = require('express'),
      router = express.Router(),
      Campaign = require('../../models/Campaign');

router.route('/').get((req, res) => {
    Campaign.find((err, campaigns) => {
        if (!err) {
            return res.json(campaigns);
        } else {
            return res.status(400).send(err);
        }
    }); 
})
.post((req, res) => {
    const campaign = new Campaign(req.body);

    campaign.save((err) => {
        if (err) {
            return res.status(400).send(err);
        }

        res.send(200);
    });
});

router.route('/:id').get((req, res) => {
    Campaign.findOne({ _id: req.params.id }, (err, campaign) => {
        if (err) {
            return res.status(400).send(err);
        }
    
        res.json(campaign);

    });
})
.patch((req,res) => {
    Campaign.findOne({ _id: req.params.id }, (err, campaign) => {

        if (err) {
            return res.status(400).send(err)
        }

        for (prop in req.body) {
            campaign[prop] = req.body[prop];
        }

        campaign.save((err) => {
            if (err) {
                return res.status(400).send(err);
            }
            res.sendStatus(200);
        })
    })
})
.delete((req,res) => {
    Campaign.remove({ _id: req.params.id }, (err) => {
        if (err) {
            return res.status(400).send(err);
        }
        res.send(200);
    });
});

router.use('/:campaignId/clients', require('./clients'));

module.exports = router;