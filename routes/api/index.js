const express = require('express')
const router = express.Router()


router.use('/campaigns', require('./campaigns'))
router.use('/taskqueue', require('./taskqueue'))
router.use('/users', require('./users'))

module.exports = router;
