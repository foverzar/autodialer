const express = require('express');
const router = express.Router();
const Campaign = require('../../models/Campaign');
const codesDict = {
    'done': 3,
    'redial': 2,
}

router.post('/lease', (req, res) => {
    Campaign.findOneAndUpdate({'isEnabled': true,  'clients.contact_status_code': 0}, {
            '$set': { 'clients.$.contact_status_code': 1 },
        },
        {
            //new: true, BUG!
            projection: {
                clients: {
                    '$elemMatch': {contact_status_code: 0},
                },
            },
            // sort: {},
        },
        (err, campaign) => {
            if (err) {
                return res.send(err);
            }

            if (!campaign) {
                return res.sendStatus(204)
            }

            res.json({
                campaignId: campaign._id,
                clientId: campaign.clients[0]._id,
                contact: campaign.clients[0].contact,
            });
        }
    );
});

router.post('/report', (req, res) => {
    const campaignId = req.query.campaignId
    const clientId = req.query.clientId

    Campaign.findOneAndUpdate({'_id': campaignId,  'clients._id': clientId}, {
            '$set': { 'clients.$.contact_status_code': codesDict[req.body.status] },
        },
        (err, campaign) => {
            if (err) {
                return res.send(err)
            }

            return res.sendStatus(200)
        }
    );
});

module.exports = router;