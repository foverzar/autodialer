const express = require('express'),
      router = express.Router({ mergeParams: true }),
      Campaign = require('../../models/Campaign');

router.route('/').get((req, res) => {
    Campaign.findById(req.params.campaignId, (err, campaign) => {
        if (!err) {
            return res.json(campaign.clients);
        } else {
            return res.send(err);
        }
    }); 
})
.post((req, res) => {
    Campaign.findById({ _id: req.params.campaignId}, (err, campaign) => {
        campaign.clients.push(req.body);

        campaign.save((err) => {
            if (err) {
                return res.send(err);
            }

            res.send(200);
        });
    });
});

router.route('/:clientId').get((req, res) => {
    Campaign.findOne({_id: req.params.campaignId,
    'clients._id': req.params.clientId}, {'clients.$': true}, (err, campaign) => {
        if (err) {
            return res.send(err);
        }
    
        res.json(campaign.clients[0]);

    });
})
.patch((req,res) => {
     Campaign.findOne({ _id: req.params.campaignId, 'clients._id': req.params.clientId}, (err, campaign) => {
        if (err) {
            return res.send(err)
        }

        for (prop in req.body) {
            campaign.clients[0][prop] = req.body[prop];
        }

        campaign.save((err) => {
            if (err) {
                return res.send(err);
            }
        });

        res.send(200);

    });
})
.delete((req,res) => {
    Campaign.remove({ _id: req.params.id }, (err) => {
        if (err) {
            return res.send(err);
        }
        res.send(200);
    });
});

module.exports = router;