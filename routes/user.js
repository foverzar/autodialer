const express = require('express'),
      router = express.Router(),
      passport = require('passport');

router.get('/login', function(req, res) {
    res.render('login', { user : req.user });
});

router.post('/login', passport.authenticate('local', { 
    successRedirect: '/',
    failureRedirect: '/user/login',
}))

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/user/login');
});

module.exports = router;
