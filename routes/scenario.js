const express = require('express');
const router = express.Router();
const twig = require('twig').twig;
const Campaign = require('../models/Campaign');


router.get('/', function(req, res, next) {
    const campaignId = req.query.campaignId;
    const clientId = req.query.clientId;
    Campaign.findOne({'_id': campaignId,  'clients._id': clientId}, { 'template': true, 'clients.$': true}, (err, campaign) => {
        if (err) {
            return res.send(err);
        }
    
	console.log(campaign)
	
	const client = JSON.parse(JSON.stringify(campaign.clients[0]))
        
        console.log(client)        

        const scenario = twig({ data: campaign.template }).render(client);

        res.render('scenario', { data: scenario });

    });
    
});

module.exports = router;
